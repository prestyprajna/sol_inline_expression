﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Inline_Expression_Task.App_Code
{
    public class PersonEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}