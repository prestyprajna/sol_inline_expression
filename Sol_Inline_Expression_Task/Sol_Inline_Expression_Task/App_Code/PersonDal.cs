﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Inline_Expression_Task.App_Code
{
    public class PersonDal
    {
        public IEnumerable<PersonEntity> GetPersonData()
        {
            return new List<PersonEntity>()
                {
                    new PersonEntity()
                    {
                        FirstName="presty",
                        LastName="prajna"
                    },
                    new PersonEntity()
                    {
                        FirstName="diwik",
                        LastName="suvarna"
                    }

                };
        }
    }
}