﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_Inline_Expression_Task.Default" %>
<%@ Import Namespace="Sol_Inline_Expression_Task.App_Code" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
        .labelSpan
        {
            background-color:slateblue;
            color:white;
            padding:60px;            
            float:left;    
            border-radius:200px;
            width:40px;
            height:40px;  
            margin:30px;                        
        }

        /*.divStyle{
            margin:50px;
        }*/

    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>

        <%--code block expression--%>

        <%
            var personListObj = new PersonDal().GetPersonData();

        %>

        <%
            foreach (var val in personListObj)
            {
             %>

        <div>
            <span class="labelSpan"><% Response.Write((Server.HtmlEncode(val.FirstName)));%>  </span>
             <span class="labelSpan"><% Response.Write((Server.HtmlEncode(val.LastName)));%>  </span>
        </div>

        <%
            }
             %>

            
    </div>
    </form>
</body>
</html>
